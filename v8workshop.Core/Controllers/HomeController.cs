﻿using System.Web.Mvc;
using Umbraco.Core.Logging;
using Umbraco.Web.Models;
using Umbraco.Web.Mvc;

namespace v8workshop.Core.Controllers
{

    public class HomeController : RenderMvcController
    {
        public ActionResult Index(ContentModel model)
        {
            Logger.Info<HomeController>($"{model.Content.Id} value of sugar is {Request.Params["sugar"]}");
            return CurrentTemplate(model);
        }
    }

}
